# Project Presentation

* [Description](#description)
* [GitLab Subproject Presentation](#gitlab-subproject-presentation)
* [Team](#team)
* [Folder and Files Structure](#folder-and-files-structure)
* [General Architecture](#general-architecture)
* [Installation and Execution](#installation-and-execution)
* [Container](#container)
  * [Folder and Files Structure Container](#folder-and-files-structure-container)
  * [Installation and Execution Container](#installation-and-execution-container)
  * [Usage Example](#usage-example)
* [Patient Manager](#patient-manager)
  * [Description](#description)
  * [Folder and Files Structure Patient Manager](#folder-and-files-structure-patient-manager)
  * [Installation and Execution Patient Manager](#installation-and-execution-patient-manager)
  * [APIs](#apis)
    * [REST APIs](#rest-apis)
      * [Methods](#methods)
  * [Components](#components)
  * [Usage Examples and Tutorial](#usage-example-and-tutorial)
* [Highlights](#highlights)
  * [React](#react)
  * [Material UI](#material-ui)
  * [Webpack](#webpack)
  * [Microfrontend](#microfrontend)

## Description

The Presentation project, with **Patient Manager** and **Container** projects, is a project based on the [Micro Frontend](https://martinfowler.com/articles/micro-frontends.html) architecture. We choose this architecture because it meets the needs of the project. The principal feature this architecture is the possibility that insert a new feature in another application with low coupling, e.g. without great dependency from each other.

The proposal of the project is adding a new feature in an existent project. In our case a Hospital Patient Management System, here called as "Container". Our feature is called **Patient Manager** that gets the next patient ID to be attended from the [SUnDaE API](https://gitlab.com/open-dish/sundae), then gets the patient information like Encounters, Diagnostics, Medications, Images, Allergies and Procedures from [Manager API](https://gitlab.com/open-dish/manager). To make it possible we used the [Module Federation Plugin](https://webpack.js.org/concepts/module-federation/) from [Webpack](https://webpack.js.org/) that attended our issue with mastery.

Webpack is a bundle that offer a lot of plugins with features for different possibilities. In our job Webpack works like a server for a CDN file, short for Content Delivery Network, but with the difference that the file is an entire application that works outside the shell application dynamically. The shell application, here the **Container**, receive the **Patient Manager** and expose its features as your own resource, but both applications are independently. If Patient Manager shot down, anything happen with the Container application, only the resource of Patient Manager is lost.

You can try **Container** at [container.app.open-dish.lab.ic.unicamp.br](https://container.app.open-dish.lab.ic.unicamp.br/) and **Patient Manager** at [patient-manager.app.open-dish.lab.ic.unicamp.br](https://patient-manager.app.open-dish.lab.ic.unicamp.br).


## GitLab Subproject Presentation
Presentation is a subproject of [Open-Dish](https://gitlab.com/open-dish/). The project is available in the [Presentation](https://gitlab.com/open-dish/presentation) repository.

## Team
* **Lucas Magri Bueno**
  * Creation of the basic structure of the Patient Manager using React components, with REST requests. Creation of Container that simulates AGHUse.
* **Marco Aurélio de O. Martins**
  * Creation of React components in Patient Manager that gets Images, Diagnostics, Allergies, Medications, Procedures, using Rest APIs. Organization, text formatting and arrangement of elements in the layout. Patient Manager documentation, Architecture and general documentation.
* **Saymon Felipe**
  * Creation of the basic structure of the project related to infrastructure such as Micro Frontend architecture with some technologies like Webpack, Docker and CI/CD pipeline. Architecture documentation.


## Folder and Files Structure

Project at [Presentation](https://gitlab.com/open-dish/presentation/):

~~~
├── packages
│   ├── container                 <- Shell Application (Micro Frontend Container)
│   │   ├── config                <- Webpack Configurations 
│   │   │   
│   │   ├── dist                  <- Bundle files make by Webpack
│   │   ├── Dockerfile            <- Docker file to build and run application
│   │   ├── node_modules
│   │   ├── package.json
│   │   ├── package-lock.json
│   │   ├── public
│   │   │   └── index.html         <- Template
│   │   └── src                    <- Source Code
│   │       
│   ├── docker-compose.yaml
│   └── patient-manager            <- Micro Frontend Patient Manager (See details in the Patient Manager topic.)
├── compose.yml
├── package-lock.json
└── README.md
~~~

## General Architecture
[Micro Frontend Architecture](https://martinfowler.com/articles/micro-frontends.html) comes from the idea of Microservice Architecture. How in the MS architecture, the objective is to isolate some functionalities of the project, so times with different skills can work on different parts of the project.

Another vantage of this architecture is the possibility of adding some new features without stopping the entire application. In the same way, is possible to update some features or remove them.

![MFE Architecture Example](doc-images/MFE.png)

> Image by [The Art of Micro Frontends](https://www.packtpub.com/product/the-art-of-micro-frontends/9781800563568)

As you can see in the figure above, **Server 1** offer the shell part of the application and the **Server 2** offer the Button of the application. Both work together to form an application.

See a diagram of MFE in this project in topic [Microfrontend](#microfrontend) below. 


## Installation and Execution
Clone the [Presentation](https://gitlab.com/open-dish/presentation) repository, access the `packages` folder:

~~~shell
git clone https://gitlab.com/open-dish/presentation.git
cd presentation/packages/
~~~

Then run it with Docker or Node Package Manager.

**Using Docker**

- First you need to get `docker` and `docker-compose`.
- [Click here](https://docs.docker.com/get-docker/) to see how you can install both.

~~~shell
docker-compose build && docker-compose up
~~~

**To run the Micro Frontends isolated with NPM**

First [install](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) `npm`.

* Container

~~~shell
cd presentation/packages/container/
npm install 
npm run start
~~~

* Patient Manager

~~~shell
cd presentation/packages/patient-manager/
npm install
npm run start
~~~

## Container
The shell application, here the **Container** application, receive the **Patient Manager** and expose its features as your own resource, but both application are independently. If Patient Manager shot down, anything happen with the Container application, only the resource of Patient Manager is lost.

You can try **Container** at [container.app.open-dish.lab.ic.unicamp.br](https://container.app.open-dish.lab.ic.unicamp.br/).


### Folder and Files Structure Container

Project at [container](https://gitlab.com/open-dish/presentation/-/tree/main/packages/container):

~~~
└── container
    ├── README.md           <- Container presentation, instructions for installation and execution
    │
    ├── config              <- Webpacks files
    │
    ├── public              
    │   └── index.html      <- Main Container html file
    │
    └── src                 <- source code in React JS
        ├── components      <- the main components of Container
        └── api.js          <- REST APIs

~~~

### Installation and Execution Container
Clone the [Presentation](https://gitlab.com/open-dish/presentation) repository, access the `container` folder:

~~~shell
git clone https://gitlab.com/open-dish/presentation.git
cd presentation/packages/container
~~~

Then run it with Node Package Manager.

- First [install](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) `npm`.

~~~shell
npm install
npm run start
~~~

### Usage Example
After [installing](#installation-and-execution) and running the Container and Patient Manager projects, or accessing [container.app.open-dish.lab.ic.unicamp.br](https://container.app.open-dish.lab.ic.unicamp.br/), you can see the Patient Manager component on the Patients page, as in [container.app.open-dish.lab.ic.unicamp.br/patients](https://container.app.open-dish.lab.ic.unicamp.br/patients).

<details><summary>Click here to see Patient Manager in Container</summary>

![Patient Manager in Container](doc-images/container.png)

</details>


## Patient Manager

### Description
The Patient Manager project is a project that gets the next patient ID (from [SUnDaE](https://gitlab.com/open-dish/sundae) team), then gets the patient information, Encounters, Diagnostics, Medications, Images, Allergies and Procedures (from [Manager](https://gitlab.com/open-dish/manager) team).

The project uses [React](https://reactjs.org/docs/getting-started.html), [Webpack](https://webpack.js.org/) and [Material UI](https://mui.com/material-ui/getting-started/overview/), an open-source React component library that implements Google's Material Design.

You can try Patient Manager at [patient-manager.app.open-dish.lab.ic.unicamp.br](https://patient-manager.app.open-dish.lab.ic.unicamp.br).


### Folder and Files Structure Patient Manager

Project at [patient-manager](https://gitlab.com/open-dish/presentation/-/tree/main/packages/patient-manager):

~~~
└── patient-manager
    ├── README.md           <- Patient Manager presentation, instructions for installation and execution
    │
    ├── config              <- Webpacks files
    │
    ├── public              
    │   └── index.html      <- Main Patient Manager html file
    │
    └── src                 <- source code in React JS
        ├── components      <- the main components of Patient Manager
        └── api.js          <- REST APIs

~~~

### Installation and Execution Patient Manager
Clone the [Presentation](https://gitlab.com/open-dish/presentation) repository, access the `patient-manager` folder:

~~~shell
git clone https://gitlab.com/open-dish/presentation.git
cd presentation/packages/patient-manager
~~~

Then run it with Docker or Node Package Manager.

**Using Docker**
- First you need to get `docker` and `docker-compose`.
- [Click here](https://docs.docker.com/get-docker/) to see how you can install both.

~~~shell
docker-compose build && docker-compose up
~~~

**Alternatively using Node Package Manager**
- First [install](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) `npm`.

~~~shell
npm install
npm run start
~~~

### APIs
### REST APIs

The Patient Manager project performs [SUnDaE](https://gitlab.com/open-dish/sundae) and [Manager](https://gitlab.com/open-dish/manager) REST requests. 

- [SUnDaE](https://gitlab.com/open-dish/sundae) is available at [sundae.api.open-dish.lab.ic.unicamp.br](https://sundae.api.open-dish.lab.ic.unicamp.br/).
- [Manager](https://gitlab.com/open-dish/manager) is available at [dhim.api.open-dish.lab.ic.unicamp.br](https://dhim.api.open-dish.lab.ic.unicamp.br/).

All methods except `getNextPacient` follow the following format using [Axios](https://axios-http.com/docs/intro):
```javascript
export const <method_title> = async (patientID, sucessCallback, errorCallback = null) => {
    return await axios.get(`endpoint`)
    .then(sucessCallback)
    .catch(errorCallback ? errorCallback : defaultErrorHandler);
}
```

The project uses [Axios](https://axios-http.com/docs/intro). Axios is a **promise-based** HTTP Client for `node.js` and the browser. On the server-side it uses the native `node.js` `http module`, while on the client (browser) it uses `XMLHttpRequests`.

#### Methods

Click and access the information of each method below.

<details><summary>Get Next Patient</summary>

**Method title**

`getNextPacient`

```plaintext
GET "https://sundae.api.open-dish.lab.ic.unicamp.br/next-patient"
```

**Response**

If successful, returns `202` and the following response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `id`              | string | Id of the next patient. |

**Example response**
```json
{
  "id": "e4ccb47f-69eb-34f1-3672-492ad7d6a9cc"
}
```
</details>

<details><summary>Get Patient Info</summary>

**Method title**

`getPatientInfo`

```plaintext
GET "https://dhim.api.open-dish.lab.ic.unicamp.br/patient/{patientID}"
```

**Response**

If successful, returns `200` and the following response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | string | Database patient id. |
| `id`              | string | Id of the next patient. |
| `birthDate`              | string | Birth date of the next patient. |
| `deathDate`              | string | Death date of the next patient. |
| `first`              | string | First name of the next patient. |
| `last`              | string | Last name of the next patient. |
| `ethnicity`              | string | Ethnicity of the next patient. |
| `gender`              | string | Gender of next patient. |
| `lat`              | string | Latitude of the next patient. |
| `lon`              | string | Longitude of the next patient. |


**Example response**
```json
{
  "_id": "48a48bf9-948e-463c-8c1e-f3d32f015c1b",
  "id": "48a48bf9-948e-463c-8c1e-f3d32f015c1b",
  "birthDate": "1989-06-05",
  "deathDate": "nan",
  "first": "Myrle",
  "last": "Bogisich",
  "race": "white",
  "ethnicity": "nonhispanic",
  "gender": "F",
  "birthPlace": "Haverhill  Massachusetts  US",
  "lat": "42.33292510523968",
  "lon": "-71.32397098150125"
}
```
</details>

<details><summary>Get Patient Encounters</summary>

**Method title**

`getPatientEncounters`

```plaintext
GET "https://dhim.api.open-dish.lab.ic.unicamp.br/encounter/patient/{patientID}"
```

**Response**

If successful, returns `200` and the following response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | string | Database encounter id. |
| `id`              | string | Id of the encounter. |
| `start`              | string | Start time of the encounter. |
| `stop`              | string | Encounter end time. |
| `patient`              | string | Id of the patient. |
| `participantActor`              | string | Id of the physician. |
| `encounterClass`              | string | Encounter type. |
| `code`              | string | Encounter type code. |
| `description`              | string | Description of the encounter |
| `reasonDescription`              | string | Description of the encounter reason. |

**Example response**
```json
[
  {
    "_id": "d2599e19-65e2-accb-6440-2ac3a77c8d5c",
    "id": "d2599e19-65e2-accb-6440-2ac3a77c8d5c",
    "start": "2000-03-21T23:25:13Z",
    "stop": "2000-03-21T23:40:13Z",
    "patient": "48a48bf9-948e-463c-8c1e-f3d32f015c1b",
    "participantActor": "26b9eb4f-9cee-337d-856d-3646035c2df7",
    "encounterClass": "ambulatory",
    "code": "185345009",
    "description": "Encounter for symptom",
    "reasonCode": "36971009.0",
    "reasonDescription": "Sinusitis (disorder)"
  },
  ...
]
```
</details>


<details><summary>Get Patient Medications</summary>

**Method title**

`getPatientMedications`

```plaintext
GET "https://dhim.api.open-dish.lab.ic.unicamp.br/medication/patient/{patientID}"
```

**Response**

If successful, returns `200` and the following response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | string | Database medication id. |
| `start`              | string | Medication use start. |
| `stop`              | string | Medication use end. |
| `patient`              | string | Id of the patient. |
| `encounter`              | string | Id of associated encounter. |
| `code`              | string | Medication type code. |
| `description`              | string | Description of the medication. |
| `reasonDescription`              | string | Description of the medication reason. |

**Example response**
```json
[
  {
    "_id": "6397982b59644f782794113b",
    "start": "2014-08-28T01:25:13Z",
    "stop": "2015-08-26T23:34:41Z",
    "patient": "48a48bf9-948e-463c-8c1e-f3d32f015c1b",
    "encounter": "702ed717-a68e-de5a-3593-810eef27022b",
    "code": "807283",
    "description": "Mirena 52 MG Intrauterine System",
    "dispenses": "12",
    "reasonCode": "nan",
    "reasonDescription": "nan"
  },
  ...
]
```
</details>

<details><summary>Get Patient Allergies</summary>

**Method title**

`getPatientAllergies`

```plaintext
GET "https://dhim.api.open-dish.lab.ic.unicamp.br/allergy/patient/{patientID}"
```

**Response**

If successful, returns `200` and the following response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | string | Database allergy id. |
| `start`              | string | Allergy start. |
| `stop`              | string | Allergy end. |
| `patient`              | string | Id of the patient. |
| `encounter`              | string | Id of associated encounter. |
| `code`              | string | Allergy type code. |
| `description`              | string | Description of the allergy. |
| `category`              | string | Category of the allergy reason. |
| `reaction1`              | string | Allergy reaction 1. |
| `severity1`              | string | Allergy severity of the reaction 1. |
| `description1`              | string | Allergy reaction 1 description. |

**Example response**
```json
[
    {
      "_id": "638f76cc00368207b78ab008",
      "start": "2004-04-26",
      "stop": "nan",
      "patient": "695dffc8-fc6a-a5b3-8f62-ffb59fc17054",
      "encounter": "54017ba5-50d6-3188-a540-a9da73f144a9",
      "code": "260147004",
      "system": "Unknown",
      "description": "House dust mite (organism)",
      "type": "allergy",
      "category": "environment",
      "reaction1": "nan",
      "description1": "nan",
      "severity1": "nan",
      "reaction2": "nan",
      "description2": "nan",
      "severity2": "nan"
    },
  ...
]
```
</details>


<details><summary>Get Patient Diagnostics</summary>

**Method title**

`getPatientDiagnostics`

```plaintext
GET "https://dhim.api.open-dish.lab.ic.unicamp.br/diagnostic/patient/{patientID}"
```

**Response**

If successful, returns `200` and the following response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | string | Database diagnostic id. |
| `id`              | string | Diagnostic id. |
| `patient`              | string | Id of the patient. |
| `encounter`              | string | Id of associated encounter. |
| `status`              | string | Diagnostic status. |
| `result`              | object | Result objects. |
| `observation`              | array | Observations. |
| `conclusionCode`              | array | Conclusion codes. |
| `reliability`              | string | Conclusion codes reliability. |

**Example response**
```json
[
    {
    "_id": "726850a4-a017-fb06-7aa2-7630c0a6cffa",
    "id": "726850a4-a017-fb06-7aa2-7630c0a6cffa",
    "patient": "48a48bf9-948e-463c-8c1e-f3d32f015c1b",
    "encounter": "d2599e19-65e2-accb-6440-2ac3a77c8d5c",
    "status": "final",
    "resultsInterpreter": "26b9eb4f-9cee-337d-856d-3646035c2df7",
    "result": {
      "observation": []
    },
    "study": [
      ""
    ],
    "conclusionCode": [
      40055000
    ],
    "reliability": [
      1
    ]
  },
  ...
]
```
</details>

<details><summary>Get Patient Images</summary>

**Method title**

`getPatientImages`

```plaintext
GET "https://dhim.api.open-dish.lab.ic.unicamp.br/image/patient/{patientID}"
```

**Response**

If successful, returns `200` and the following response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | string | Database diagnostic id. |
| `id`              | string | Diagnostic id. |
| `patient`              | string | Id of the patient. |
| `date`              | string | Image creation date. |
| `encounter`              | string | Encounter associated id. |
| `imagePath`              | string | Image relative path. |
| `boxesInfo`              | Object array | Observations. |

**Example response**

Note: `boxesInfo` has not been implemented by other teams. There are no `boxesInfo` in the images database. But Patient Manager load them from a [mock](https://run.mocky.io/v3/1aad8200-2ac9-4f24-9dd3-b2e449ffa247).

```json
[
    {
    "_id": "1d88a815-fbc2-9b61-b582-944100e41fd1",
    "id": "1d88a815-fbc2-9b61-b582-944100e41fd1",
    "date": "2003-11-01T20:30:34Z",
    "patient": "ada32eac-0c43-38d0-14e5-e2e545631eef",
    "encounter": "13efcb79-c259-e4a5-2258-ea655ceeeb48",
    "imagePath": "images/blood-cell/1d88a815-fbc2-9b61-b582-944100e41fd1.png",
    "annotationRaw": null,
    "boxesInfo": [
      {
        "label": "RBC",
        "confidence": "0.28805845975875854",
        "topLeft": {
          "x": "0",
          "y": "0"
        },
        "bottomRight": {
          "x": "71",
          "y": "87"
        }
      },
      {
        "label": "Teste",
        "confidence": "0.982",
        "topLeft": {
          "x": "200",
          "y": "250"
        },
        "bottomRight": {
          "x": "250",
          "y": "300"
        }
      }
    ]
  },
  ...
]
```
</details>

There are also other methods similar to the methods described above, but perform queries by Encounter id:
- `getPatientMedicationsOnEncounter`, 
- `getPatientProceduresOnEncounter`, 
- `getPatientAllergiesOnEncounter`, 
- `getPatientImagesOnEncounter`,
- `getPatientDiagnosticsOnEncounter`. 

When the GET request does not find what it is looking for, it returns the code `404` and the message:

**Example response**
```json
{
  "detail": "Image doesn't exist."
}
```

**Note**

Pagination has been implemented in [Manager](https://gitlab.com/open-dish/manager), but only in endpoints like GET all patients, all medications... The Patient Manager does not use these endpoints because there is no functionality that lists certain data without specifying a patient ID, for example.

A suggestion for the upcoming Manager team would be to apply the pagination system also to endpoints that search for information on a specific patient as this would avoid returning too much data at once.

### Components
The project is divided into multiple components, avoiding a monolithic app. See each component's dependency and a brief description of each component type.

<details><summary>Click here to see component's dependency</summary>

![Components](doc-images/components.png)

</details>


### Usage Example and Tutorial

You can try Patient Manager at [patient-manager.app.open-dish.lab.ic.unicamp.br](https://patient-manager.app.open-dish.lab.ic.unicamp.br).

<details><summary>Click here to see usage examples</summary>


**Start and next patient ID**

The system starts with the click on the "Who's the next patient?" button. Then the ID of that next patient is shown.

![Next patient ID](doc-images/next-patient-id.png)

**Next patient information**

Clicking on the magnifying glass shows the next patient's information.

![Next patient information](doc-images/patient-info.png)

**Next patient Encounters, Diagnostics, Medications, Allergies and Images**

Clicking the magnifying glass again shows Encounters, Diagnostics, Medications, Allergies, Images of the next patient, in tabs.

![Patient Encounters](doc-images/patient-encounters.png)

**Next patient encounter**

Clicking on an encounter shows the items related to the encounter and Procedures for the next patient, also in tabs.

![Patient Encounter Information](doc-images/patient-encounter-info.png)

**Go to next patient**

The process is restarted by clicking on "Search new patient" button, at the bottom of the page.

**Animated usage example**

![Animated usage example](doc-images/usage.gif)


</details>

# Highlights

## React
The Patient Manager project uses [React](https://reactjs.org/) as an open-source front-end JavaScript library to create interactive user interfaces. As we could see in the [Components](#components) topic, with React it was possible to build encapsulated components that manage their own state, then compose them to make complex UIs.

See an example of encapsulating `NextPacient` and `PacientView` in `PacientManager` using React:

```javascript
export const PacientManager = () => {
    ...
    return (
        <Box>
            <NextPacient pacient={nextPacient} setPacient={setNextPacient}/>
            <PacientView pacient={nextPacient} setPacient={setNextPacient}/>
        </Box>
    )
}
```
This way, we have a code much more modern, organized, predictable and easier to debug. During the development of the project, it was possible to notice how the development was accelerated because of the use of React and Material UI below.

## Material UI
The project interface uses [Material UI](https://mui.com/material-ui/getting-started/overview/), an open-source React component library that includes a comprehensive collection of prebuilt components, ready for use in production as the short time for the development of the project required. Material UI features a suite of customization options that make it easy to custom components.

Below is an example of importing Material UI and an example of usage. 

<details><summary>Click here to see an implemented example</summary>

Here we use a large collection of UI features (`Box`, `Collapse`, `List`, `ListItemButton`, ...) in a few lines of code. We also have some customization examples.

```javascript
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
...
import ExpandMore from '@mui/icons-material/ExpandMore';
import HealingIcon from '@mui/icons-material/Healing';
...
proceduresOnEncounter.map((procedure, index) => (
  <Box key={`procedure-on-encounter-${index}`}>
      <Divider />
      <ListItemButton onClick={() => handleOpenListItem(`procedure-on-encounter-${index}`)}>
          <ListItemIcon>
              <HealingIcon />
          </ListItemIcon>
          <ListItemText primary={`Code: ${procedure.code}, Reason code: ${procedure.reasonCode}`} />
          {openListItem === `procedure-on-encounter-${index}` ? <ExpandLess /> : <ExpandMore />}
      </ListItemButton>
      <Collapse in={openListItem === `procedure-on-encounter-${index}`} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
              <ListItem>
                  {`Start: ${validDate(procedure.start, "hh:mm, dd/mm/yyyy")}`}
              </ListItem>
              ...
          </List>
      </Collapse>
  </Box>
))
```

</details>

## Webpack

An issue that comes up when we build a JavaScript project is that we need to import all dependencies one by one and in the correct order. [Webpack](https://webpack.js.org/) solve our problems because it takes modules with dependencies and generates static assets representing those modules, and it will put all code in one bundle file, called here `bootstrap.js` or `remoteEntry.js`.

See an example applied to this project. Inside Patient Manager project, Webpack is configured to **expose** the `PatientManagerApp`:

```javascript
...
plugins: [
    new ModuleFederationPlugin({
        name: 'patient_manager',
        filename: 'remoteEntry.js',
        exposes: {
            './PatientManagerApp': './src/bootstrap',
        }
    })
]
...
```

The `./src/bootstrap.js` file is a local root file of the app. Already the `./PatientManagerApp` is how you can find the Micro Frontend remotely.

Inside the Container project, Webpack is configured to receive the remote `PatientManagerApp`:

```javascript
...
plugins: [
    new ModuleFederationPlugin({
        name: 'container',
        remotes: {
            patient_manager: 'patient_manager@https://patient-manager.app.open-dish.lab.ic.unicamp.br/remoteEntry.js',
        }
    }),
]
...
```

As you can see below, now the `PatientManagerApp` can be imported like any package in Container project. Here it received a new name `mount` and is exported like a normal React component.

* `PatientManager.js` file in Container:

```javascript
import { mount } from 'patient_manager/PatientManagerApp';
...
export default () => {
  ...
  useEffect(() => {
    mount(ref.current);
  });
  return (<div ref={ref} />);
};
```

After that the `PatientManager.js` can be used like a component in Container project.

<details><summary>Click here to see an implemented example</summary>

``` javascript
...
import PatientManager from './PatientManager';
...
export const Patients = () => {
  return (
    <Card>
      <CardHeader
        title="Pacientes"
        subheader="Bem-vindo [...]. Clique no botão abaixo para inicar o atendimento ao próximo paciente."
      />
      ...
      <CardContent>	
        <PatientManager />
      </CardContent>
    </Card>
  )
}
```

</details>


See below a diagram of using Webpack in the project.


<details><summary>Click here to see Webpack diagram</summary>

![Webpack](doc-images/webpack.png)

</details>



## Microfrontend
Microfrontend was important for this project because it allowed the decoupling of the Container and Patient Manager components very easily. We believe that this architecture in the project should be continued because it perfectly meets the interoperability issues discussed in class.

See below how adding a third component (named "Component 2") does not affect the pre-existing Patient Manager and Container components.

![MFE Architecture Components](doc-images/MFE-Components.png)