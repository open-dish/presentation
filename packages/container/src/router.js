import React from 'react';
import { createBrowserRouter, createRoutesFromElements, Route } from "react-router-dom";

import { Container } from './components/Container';
import { QuickAccess } from './components/QuickAccess';
import { CovidManagement } from './components/CovidManagement';
import { Patients } from './components/Patients';

export const getRouter = (appColorMode) => {
	return createBrowserRouter(
		createRoutesFromElements(
			<Route path="/" element={ <Container colorMode={appColorMode} /> }>
				<Route path="quickAccess" element={ <QuickAccess /> } />
				<Route path="covidManagement" element={ <CovidManagement /> } />
				<Route path="patients" element={ <Patients /> } />
			</Route>
		)
	);
}