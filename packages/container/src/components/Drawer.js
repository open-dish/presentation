import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import MuiDrawer from '@mui/material/Drawer';
import Divider from '@mui/material/Divider';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import IconButton from '@mui/material/IconButton';

import { DrawerHeader } from './DrawerHeader';
import { DrawerButtonsList } from './DrawerButtonsList';
import { Outlet } from 'react-router-dom';

const drawerWidth = 240;

const openedMixin = (theme) => ({
	width: drawerWidth,
	transition: theme.transitions.create('width', {
	easing: theme.transitions.easing.sharp,
	duration: theme.transitions.duration.enteringScreen,
	}),
	overflowX: 'hidden',
});
  
const closedMixin = (theme) => ({
	transition: theme.transitions.create('width', {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.leavingScreen,
	}),
	overflowX: 'hidden',
	width: `calc(${theme.spacing(7)} + 1px)`,
	[theme.breakpoints.up('sm')]: {
		width: `calc(${theme.spacing(8)} + 1px)`,
	},
});

const DrawerTemplate = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
	({ theme, open }) => ({
	width: drawerWidth,
	flexShrink: 0,
	whiteSpace: 'nowrap',
	boxSizing: 'border-box',
	...(open && {
		...openedMixin(theme),
		'& .MuiDrawer-paper': openedMixin(theme),
	}),
	...(!open && {
		...closedMixin(theme),
		'& .MuiDrawer-paper': closedMixin(theme),
	}),
	}),
);

export const Drawer = ({ isOpen, setIsOpen }) => {
	const theme = useTheme();
	return (
		<DrawerTemplate variant="permanent" open={isOpen}>
			<DrawerHeader>
				<IconButton onClick={() => setIsOpen(false)}>
					{
						theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />
					}
				</IconButton>
			</DrawerHeader>
			<Divider />
			<DrawerButtonsList isDrawerOpen={isOpen} />
		</DrawerTemplate>
	)
}