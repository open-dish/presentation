import * as React from 'react';
import { Chart } from "react-charts";

const mapRawData = (rawData) => {
    return [
        {
            label: "Casos de Covid-19",
            data: rawData.map(data => (
                {
                    label: data.state,
                    cases: data.cases
                }
            ))
        },
        {
            label: "Suspeitas de Covid-19",
            data: rawData.map(data => (
                {
                    label: data.state,
                    cases: data.suspects
                }
            ))
        },
        {
            label: "Mortes por Covid-19",
            data: rawData.map(data => (
                {
                    label: data.state,
                    cases: data.deaths
                }
            ))
        },
    ]
}

export const CovidChart = ({ rawData }) => {
    const data = mapRawData(rawData);

    const primaryAxis = React.useMemo(
        () => ({
          getValue: datum => datum.label,
        }),
        []
    )
    
    const secondaryAxes = React.useMemo(
        () => [{
            getValue: datum => datum.cases,
        }],
        []
    )
    return (
        <Chart 
            options={{
                data,
                primaryAxis,
                secondaryAxes,
            }}
        />
    )
}