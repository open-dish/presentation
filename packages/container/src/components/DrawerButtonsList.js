import * as React from "react";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Tooltip from '@mui/material/Tooltip';
import ViewQuiltIcon from '@mui/icons-material/ViewQuilt';
import PeopleIcon from '@mui/icons-material/People';
import MasksIcon from '@mui/icons-material/Masks';
import { useLocation, useNavigate } from "react-router-dom";

const ButtonsList = [
	{
		onCLick: navigate => {
			navigate("quickAccess");
		},
		icon: <ViewQuiltIcon />,
		text: "Acesso Rápido",
		redirectTo: "/quickAccess"
	},
	{
		onCLick: navigate => {
			navigate("covidManagement");
		},
		icon: <MasksIcon />,
		text: "Área COVID-19",
		redirectTo: "/covidManagement"
	},
	{
		onCLick: navigate => {
			navigate("patients");
		},
		icon: <PeopleIcon />,
		text: "Pacientes",
		redirectTo: "/patients"
	}
];

const getListItemContent = (button, isDrawerOpen) => {
	const navigate = useNavigate();
	const location = useLocation();

	return (
		<ListItemButton 
			sx={{
				minHeight: 48,
				justifyContent: isDrawerOpen ? 'initial' : 'center',
				px: 2.5
			}}
			onClick={() => button.onCLick(navigate)}
			selected={ location.pathname === button.redirectTo }
			>
			<ListItemIcon
				sx={{
					minWidth: 0,
					mr: isDrawerOpen ? 3 : 'auto',
					justifyContent: 'center',
				}}
				>
				{ button.icon }
			</ListItemIcon>
			<ListItemText primary={button.text} sx={{ opacity: isDrawerOpen ? 1 : 0 }} />
		</ListItemButton>
	)
}

export const DrawerButtonsList = ({ isDrawerOpen }) => {
	return (
		<List>
			{ButtonsList.map((button, index) => (
					<ListItem key={`list-button-${index}`} disablePadding sx={{ display: 'block' }}>
						{
							isDrawerOpen ?
							getListItemContent(button, isDrawerOpen)	
							:
							<Tooltip title={button.text} placement="right">
								{ getListItemContent(button, isDrawerOpen) }
							</Tooltip>
						}
					</ListItem>
				))
			}
		</List>
	)
}