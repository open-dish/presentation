import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { useNavigate } from "react-router-dom";

import covid_button_image from '../images/covid_button_image.svg';
import patients_button_image from '../images/patients_button_image.svg';
import { ImageButton, ImageSrc, Image, ImageBackdrop, ImageMarked } from './QuickAccessImages'

const ButtonImages = [
	{
	  url: covid_button_image,
	  title: 'Área COVID-19',
	  redirectTo: "/covidManagement"
	},
	{
	  url: patients_button_image,
	  title: 'Pacientes',
	  redirectTo: "/patients"
	}
];

export const QuickAccess = () => {

	const navigate = useNavigate();
	return (
		<Box sx={{ display: 'flex', flexWrap: 'wrap', minWidth: 300, width: '100%' }}>
			{ButtonImages.map((image) => (
				<ImageButton
					focusRipple
					key={image.title}
					style={{
						flexGrow: 1
					}}
					onClick={ () => navigate(image.redirectTo) }
				>
				<ImageSrc style={{ backgroundImage: `url(${image.url})` }} />
				<ImageBackdrop className="MuiImageBackdrop-root" />
				<Image>
					<Typography
						component="span"
						variant="subtitle1"
						color="inherit"
						sx={{
							position: 'relative',
							p: 4,
							pt: 2,
							pb: (theme) => `calc(${theme.spacing(1)} + 6px)`,
						}}
					>
						{image.title}
						<ImageMarked className="MuiImageMarked-root" />
					</Typography>
				</Image>
				</ImageButton>
			))}
		</Box>
	)
}