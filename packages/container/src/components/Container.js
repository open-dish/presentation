import * as React from 'react';
import Box from '@mui/material/Box';
import { Container as MaterialContainer } from '@mui/material';

import CssBaseline from '@mui/material/CssBaseline';

import { Drawer } from './Drawer';
import { DrawerHeader } from './DrawerHeader';
import { MainToolbar } from './MainToolbar';
import { Outlet } from 'react-router-dom';

export const Container = ({ colorMode }) => {
	const [isDrawerOpen, setIsDrawerOpen] = React.useState(false);
	return (
		<Box sx={{ display: 'flex' }}>
			<CssBaseline />
			<MainToolbar colorMode={colorMode} isDrawerOpen={isDrawerOpen} setIsDrawerOpen={setIsDrawerOpen} />
			<Drawer isOpen={isDrawerOpen} setIsOpen={setIsDrawerOpen}/>
			<MaterialContainer component="main" sx={{ p: 3 }} maxWidth="xl">
				<DrawerHeader />
				<Outlet />
			</MaterialContainer>
		</Box>
	);
}