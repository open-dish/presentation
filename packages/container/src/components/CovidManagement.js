import React from 'react';
import { Card, CardMedia, CardContent, Divider, CardHeader } from '@mui/material';

import { AlertSnackBar } from './AlertSnackBar';
import { LoadingBackdrop } from './LoadingBackdrop';
import { CovidChart } from './CovidChart';
import covid_button_image from '../images/covid_button_image.svg';

import { getAllCovidCases } from '../api';

const getCovidCases = async(setCovidCases) => {
	const getSuccess = response => {
		setCovidCases(response.data.data)
	}
	await getAllCovidCases(getSuccess);
}

export const CovidManagement = () => {
	const [covidCases, setCovidCases] = React.useState(null);

	React.useEffect(() => {
		getCovidCases(setCovidCases);
	}, [])

	return (
		<>
			<Card>
				<CardMedia
					component="img"
					height="300"
					image={covid_button_image}
					alt="imagem covid"
				/>
				<CardHeader
					title="Área COVID-19"
					subheader="Bem vindo a Área COVID-19."
				/>
				<Divider />
				<CardContent sx={{ height: covidCases ? 700 : "auto", p: 0, m: 3 }}>
					{
						covidCases ? <CovidChart rawData={covidCases} /> : null
					}
				</CardContent>
			</Card>
			<LoadingBackdrop showBackdrop={!covidCases} />
			{ covidCases ? <AlertSnackBar /> : null }
		</>
	)
}