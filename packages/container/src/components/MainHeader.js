import * as React from "react";

import { styled } from '@mui/material/styles';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import MenuIcon from '@mui/icons-material/Menu';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';

import { DarkModeSwitchButton } from './DarkModeSwitchButton';

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
	shouldForwardProp: (prop) => prop !== 'open',
	})(({ theme, open }) => ({
		zIndex: theme.zIndex.drawer + 1,
		transition: theme.transitions.create(['width', 'margin'], {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.leavingScreen,
		}),
		...(open && {
		marginLeft: drawerWidth,
		width: `calc(100% - ${drawerWidth}px)`,
		transition: theme.transitions.create(['width', 'margin'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen,
		}),
	}),
}));

export const MainHeader = ({ isSideMenuOpen, setIsSideMenuOpen, colorMode }) => {

    return (
		<AppBar position="fixed" open={open}>
			<Toolbar>
			<IconButton
				color="inherit"
				aria-label="open-side-menu"
				onClick={() => setIsSideMenuOpen(!isSideMenuOpen)}
				edge="start"
				sx={{
					marginRight: 5,
					...(open && { display: 'none' }),
				}}
			>
				<MenuIcon />
			</IconButton>
			<Typography variant="h6" noWrap component="div">
				AGHUse
			</Typography>
			<DarkModeSwitchButton colorMode={colorMode} />
			</Toolbar>
		</AppBar>
    )
}