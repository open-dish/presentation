import React from 'react';
import { Card, CardMedia, CardContent, Divider, CardHeader } from '@mui/material';

import PatientManager from './PatientManager';
import patients_button_image from '../images/patients_button_image.svg';

export const Patients = () => {
	return (
		<Card>
			<CardMedia
				component="img"
				height="300"
				image={patients_button_image}
				alt="imagem de pacientes"
			/>
			<CardHeader
				title="Pacientes"
				subheader="Bem vindo a sessão de atendimento à Pacientes. Clique no botão abaixo para inicar o atendimento ao próximo paciente."
			/>
			<Divider />
			<CardContent>	
				<PatientManager />
			</CardContent>
		</Card>
	)
}