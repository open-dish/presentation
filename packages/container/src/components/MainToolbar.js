import * as React from 'react';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Typography from '@mui/material/Typography';

import { AppBar } from './AppBar';
import { DarkModeSwitchButton } from './DarkModeSwitchButton';

export const MainToolbar = ({ isDrawerOpen, setIsDrawerOpen, colorMode }) => {
	return (
		<AppBar position="fixed" open={isDrawerOpen}>
			<Toolbar>
				<IconButton
					color="inherit"
					aria-label="abrir janela de aplicativos"
					onClick={() => setIsDrawerOpen(true)}
					edge="start"
					sx={{
						marginRight: 5,
						...(isDrawerOpen && { display: 'none' }),
					}}
				>
					<MenuIcon />
				</IconButton>
					<Typography sx={{ flexGrow: 1 }} variant="h6" noWrap component="div">
						AGHUse
					</Typography>
				<DarkModeSwitchButton colorMode={colorMode} />
			</Toolbar>
		</AppBar>
	)
}