import axios from 'axios';

const covidAPIURL = "https://covid19-brazil-api.now.sh/api/report/v1";

const defaultErrorHandler = error => {
    console.log(error);
}

const defaultSuccessCallback = (successCallback, response) => {
    successCallback(response);
}

export const getAllCovidCases = async successCallback => {
    return await axios.get(`${covidAPIURL}`)
    .then(response => defaultSuccessCallback(successCallback, response))
    .catch(defaultErrorHandler);
}

export const getCovidCasesInBrazilianState = async (successCallback, uf) => {
    return await axios.get(`${covidAPIURL}/brazil/uf/${uf}`)
    .then(response => defaultSuccessCallback(successCallback, response))
    .catch(defaultErrorHandler);
}

export const getCovidCasesInBrazilByDate = async (successCallback, YYYYMMDD) => {
    return await axios.get(`${covidAPIURL}/brazil/${YYYMMDD}`)
    .then(response => defaultSuccessCallback(successCallback, response))
    .catch(defaultErrorHandler);
}