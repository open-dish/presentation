import React from 'react';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { RouterProvider } from "react-router-dom";

import { getRouter } from './router';

const ColorModeContext = React.createContext({ toggleColorMode: () => {} });

export default () => {
	const [mode, setMode] = React.useState('light');
	const colorMode = React.useMemo(
		() => ({
		toggleColorMode: () => {
			setMode((prevMode) => (prevMode === 'light' ? 'dark' : 'light'));
		},
		}),
		[]
	);

	const theme = React.useMemo(() => createTheme({
		palette: {
			mode,
			},
		}),
		[mode]
	);

	return (
		<React.StrictMode>
			<ColorModeContext.Provider value={colorMode}>
				<ThemeProvider theme={theme}>
					<RouterProvider router={getRouter(colorMode)} />
				</ThemeProvider>
			</ColorModeContext.Provider>
		</React.StrictMode>
	);
};