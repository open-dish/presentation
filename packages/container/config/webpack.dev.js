const { merge } = require('webpack-merge');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const commonConfig = require('./webpack.common');
const packageJson = require('../package.json');

const devConfig = {
    mode: 'development',
    devServer: {
        allowedHosts: "all",
        compress: false,
        host: '0.0.0.0',
        port: 8080,
        historyApiFallback: true,
    },
    plugins: [
        new ModuleFederationPlugin({
           name: 'container',
           remotes: {
               // Production Mode
               patient_manager: 'patient_manager@https://patient-manager.app.open-dish.lab.ic.unicamp.br/remoteEntry.js',
               // Development Mode
               // patient_manager: 'patient_manager@http://localhost:8083/remoteEntry.js',
           },
           shared: packageJson.dependencies
        }),
    ],
};

module.exports = merge(commonConfig, devConfig);