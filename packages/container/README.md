# Container

## Description
The shell application, here the **Container** application, receive the **Patient Manager** and expose its features as your own resource, but both application are independently. If Patient Manager shot down, anything happen with the Container application, only the resource of Patient Manager is lost.

You can try **Container** at [container.app.open-dish.lab.ic.unicamp.br](https://container.app.open-dish.lab.ic.unicamp.br/).

### GitLab Subproject Patient Container
Container is a subproject of [Open-Dish](https://gitlab.com/open-dish/). The project is available in the [Presentation](https://gitlab.com/open-dish/presentation) repository. Click [here](https://gitlab.com/open-dish/presentation) to see the full documentation.


## Folder and Files Structure Container

Project at [container](https://gitlab.com/open-dish/presentation/-/tree/main/packages/container):

~~~
└── container
    ├── README.md           <- Container presentation, instructions for installation and execution
    │
    ├── config              <- Webpacks files
    │
    ├── public              
    │   └── index.html      <- Main Container html file
    │
    └── src                 <- source code in React JS
        ├── components      <- the main components of Container
        └── api.js          <- REST APIs

~~~

## Installation and Execution Container
Clone the [Presentation](https://gitlab.com/open-dish/presentation) repository, access the `container` folder:

~~~shell
git clone https://gitlab.com/open-dish/presentation.git
cd presentation/packages/container
~~~

Then run it with Node Package Manager.

- First [install](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) `npm`.

~~~shell
npm install
npm run start
~~~