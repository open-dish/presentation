## Patient Manager

### Description
The Patient Manager project is a project that gets the next patient (from [SUnDaE](https://gitlab.com/open-dish/sundae) team), then gets the patient information, Encounters, Diagnostics, Medications, Images, Allergies and Procedures (from [Manager](https://gitlab.com/open-dish/manager) team).

The project uses [React](https://reactjs.org/docs/getting-started.html), [Webpack](https://webpack.js.org/) and [Material UI](https://mui.com/material-ui/getting-started/overview/), an open-source React component library that implements Google's Material Design.

You can try Patient Manager at [patient-manager.app.open-dish.lab.ic.unicamp.br](https://patient-manager.app.open-dish.lab.ic.unicamp.br).

### GitLab Subproject Patient Manager
Patient Manager is a subproject of [Open-Dish](https://gitlab.com/open-dish/). The project is available in the [Presentation](https://gitlab.com/open-dish/presentation) repository. Click [here](https://gitlab.com/open-dish/presentation) to see the full documentation.


### Folder and Files Structure Patient Manager

Project at [patient-manager](https://gitlab.com/open-dish/presentation/-/tree/main/packages/patient-manager):

~~~
└── patient-manager
    ├── README.md           <- Patient Manager presentation, instructions for installation and execution
    │
    ├── config              <- Webpacks files
    │
    ├── public              
    │   └── index.html      <- Main Patient Manager html file
    │
    └── src                 <- source code in React JS
        ├── components      <- the main components of Patient Manager
        └── api.js          <- REST APIs

~~~

### Installation and Execution Patient Manager
Clone the [Presentation](https://gitlab.com/open-dish/presentation) repository, access the `patient-manager` folder:

~~~shell
git clone https://gitlab.com/open-dish/presentation.git
cd presentation/packages/patient-manager
~~~

Then run it with Docker or Node Package Manager.

**Using Docker**
- First you need to get `docker` and `docker-compose`.
- [Click here](https://docs.docker.com/get-docker/) to see how you can install both.

~~~shell
docker-compose build && docker-compose up
~~~

**Alternatively using Node Package Manager**
- First [install](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) `npm`.

~~~shell
npm install
npm run start
~~~