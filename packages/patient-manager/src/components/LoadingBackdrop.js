import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';

const defaultBackdropStyles = {
    color: '#ffffff',
    zIndex: (theme) => theme.zIndex.drawer + 1
}

const defaultCircularProgressStyles = {
    color: 'inherit'
}

export const LoadingBackdrop = ({ showBackdrop, backdropStyles = null, circularProgressStyles = null }) => {
    return (
        <Backdrop sx={backdropStyles ? backdropStyles : defaultBackdropStyles} open={showBackdrop}>
            <CircularProgress sx={circularProgressStyles ? circularProgressStyles : defaultCircularProgressStyles} />
        </Backdrop>
    )
}