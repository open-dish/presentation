import { useState } from 'react';

import { Box } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import { LoadingBackdrop } from './LoadingBackdrop';

import { getNextPacient } from '../api';


export const NextPacient = ({ pacient, setPacient }) => {
    const [loading, setLoading] = useState(false);

    const findNextPacient = async () => {
        const onNextPacientLoaded = response => {setPacient({id: response.data.id});}
        setLoading(true);
        await getNextPacient(onNextPacientLoaded);
        //setPacient({id: "6e255102-de93-78fd-a76e-55093578bf21"});
        setLoading(false);
    }

    return (
        <>
            {
                pacient ? 
                null
                :
                <Box>
                    <LoadingButton
                        size="small"
                        onClick={findNextPacient}
                        loading={loading}
                        variant="contained"
                        disabled={loading}
                        
                        >
                            Who's the next patient?
                    </LoadingButton>
                    <LoadingBackdrop showBackdrop={loading} />
                </Box>
            }            
        </>
    )
}