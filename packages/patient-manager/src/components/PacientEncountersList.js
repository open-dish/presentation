import { useState } from 'react';
import * as React from 'react';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import Paper from '@mui/material/Paper';
import ArticleIcon from '@mui/icons-material/Article';
import WarningIcon from '@mui/icons-material/Warning';
import HealingIcon from '@mui/icons-material/Healing';
import MedicationIcon from '@mui/icons-material/Medication';
import ImageIcon from '@mui/icons-material/Image';
import SimCardAlertIcon from '@mui/icons-material/SimCardAlert';
import { styled } from '@mui/material/styles';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import dateFormat from 'dateformat';

import { MedicationsOnEncounterList } from './MedicationsOnEncounterList';
import { ProceduresOnEncounterList } from './ProceduresOnEncounterList';
import { AllergiesOnEncounterList } from './AllergiesOnEncounterList';
import { ImagesOnEncounterList } from './ImagesOnEncounterList';
import { DiagnosticsOnEncounterList } from './DiagnosticsOnEncounterList';
/*import { AnaliseEncounterButton } from './AnaliseEncounterButton';*/

import { getPatientMedicationsOnEncounter } from '../api.js';
import { getPatientProceduresOnEncounter } from '../api.js';
import { getPatientAllergiesOnEncounter } from '../api.js';
import { getPatientImagesOnEncounter } from '../api.js';
import { getPatientDiagnosticsOnEncounter } from '../api.js';

const pacientEncountersListStyles = {
    width: '100%',
    bgcolor: 'background.paper'
}

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

const validDate = (dateString, formatString) => {
    try {
        return dateFormat(dateString, formatString)
    } catch (error) {
        return "Not defined."
    }
}

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography  component={'span'} >{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
  };
  
  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

const getMedications = async (encounterID, onMedicationsLoaded) => {await getPatientMedicationsOnEncounter(encounterID, onMedicationsLoaded);}
const getProcedures = async (encounterID, onProceduresLoaded) => {await getPatientProceduresOnEncounter(encounterID, onProceduresLoaded);}
const getAllergies = async (encounterID, onAllergiesLoaded) => {await getPatientAllergiesOnEncounter(encounterID, onAllergiesLoaded);}
const getImages = async (encounterID, onImagesLoaded) => {await getPatientImagesOnEncounter(encounterID, onImagesLoaded);}
const getDiagnostics = async (encounterID, onDiagnosticsLoaded) => {await getPatientDiagnosticsOnEncounter(encounterID, onDiagnosticsLoaded);}



export const PacientEncountersList = ({ pacientEncounters }) => {
    const [openListItem, setOpenListItem] = useState(null);
    const [medicationsOnEncounter, setMedicationsOnEncounter] = useState([]);
    const [proceduresOnEncounter, setProceduresOnEncounter] = useState([]);
    const [allergiesOnEncounter, setAllergiesOnEncounter] = useState([]);
    const [imagesOnEncounter, setImagesOnEncounter] = useState([]);
    const [diagnosticsOnEncounter, setDiagnosticsOnEncounter] = useState([]);
    
    //Transferir isto para PacientView
    const [pacientEncountersML, setPacientEncountersML] = useState(null);
    
    const onMedicationsLoaded = response => {setMedicationsOnEncounter(response.data);}
    const onProceduresLoaded = response => {setProceduresOnEncounter(response.data);}
    const onAllergiesLoaded = response => {setAllergiesOnEncounter(response.data);}
    const onImagesLoaded = response => {setImagesOnEncounter(response.data);}
    const onDiagnosticsLoaded = response => {setDiagnosticsOnEncounter(response.data);}

    const handleOpenListItem = (encounter, clickedIndex) => {
        if (openListItem === clickedIndex) {
            setOpenListItem(null);
        } else {
            setOpenListItem(clickedIndex);
            getMedications(encounter.id, onMedicationsLoaded);
            getProcedures(encounter.id, onProceduresLoaded);
            getAllergies(encounter.id, onAllergiesLoaded);
            getImages(encounter.id, onImagesLoaded);
            getDiagnostics(encounter.id, onDiagnosticsLoaded);
        }
    };

    
    const [value, setValue] = React.useState(0);
    const handleChangeEncounterContent = (event, newValue) => {
        setValue(newValue);
    };

    return (
        
        <List
            sx={pacientEncountersListStyles}
            component="nav"
            aria-labelledby="pacient-encounters"
        >
            {
                pacientEncounters.map((encounter, index) => (
                    <Box key={index}>
                        { index > 0 ? <Divider /> : null }
                        <ListItemButton onClick={() => handleOpenListItem(encounter, index)}>
                            <ListItemIcon>
                                <ArticleIcon />
                            </ListItemIcon>
                            <ListItemText primary={validDate(encounter.start, "yyyy, mmmm dS - hh:mm")} />
                            {openListItem === index ? <ExpandLess /> : <ExpandMore />}
                        </ListItemButton>
                        <Collapse in={openListItem === index} timeout="auto" unmountOnExit>
                            <List component="div" disablePadding>
                                <ListItem>
                                    {`Reason: ${encounter.reasonDescription}. (Code: ${encounter.reasonCode})`}</ListItem>
                                <ListItem>
                                    {`Description: ${encounter.description}`}
                                </ListItem>
                                <ListItem>
                                    {`Encounter Class: ${encounter.encounterClass}`}
                                </ListItem>
                                <ListItem>
                                    {`Date: ${validDate(encounter.start, "dd/mm/yyyy")}`}
                                </ListItem>

                                <Box sx={{ width: '100%' }}>
                                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                                        <Tabs value={value} onChange={handleChangeEncounterContent} aria-label="Encounter Content">
                                        <Tab label={`Procedures (${proceduresOnEncounter.length})`} {...a11yProps(0)}  icon={<HealingIcon />} iconPosition="start"  />
                                        <Tab label={`Medications (${medicationsOnEncounter.length})`} {...a11yProps(1)}  icon={<MedicationIcon />} iconPosition="start" />
                                        <Tab label={`Allergies (${allergiesOnEncounter.length})`} {...a11yProps(2)}  icon={<WarningIcon />} iconPosition="start" />
                                        <Tab label={`Images (${imagesOnEncounter.length})`} {...a11yProps(3)}  icon={<ImageIcon />} iconPosition="start" />
                                        <Tab label={`Diagnostics (${diagnosticsOnEncounter.length})`} {...a11yProps(4)}  icon={<SimCardAlertIcon />} iconPosition="start" />
                                        </Tabs>
                                    </Box>
                                    <TabPanel value={value} index={0}>
                                        {<ProceduresOnEncounterList proceduresOnEncounter={proceduresOnEncounter}/>}
                                    </TabPanel>
                                    <TabPanel value={value} index={1}>
                                        {<MedicationsOnEncounterList medicationsOnEncounter={medicationsOnEncounter}/>}
                                    </TabPanel>
                                    <TabPanel value={value} index={2}>
                                        {<AllergiesOnEncounterList allergiesOnEncounter={allergiesOnEncounter}/>}
                                    </TabPanel>
                                    <TabPanel value={value} index={3}>
                                        {<ImagesOnEncounterList imagesOnEncounter={imagesOnEncounter}/>}
                                    </TabPanel>
                                    <TabPanel value={value} index={4}>
                                        {<DiagnosticsOnEncounterList diagnosticsOnEncounter={diagnosticsOnEncounter}/>}
                                    </TabPanel>
                                </Box>

                            </List>
                        </Collapse>
                    </Box>
                ))
            }
        </List>
    )
}