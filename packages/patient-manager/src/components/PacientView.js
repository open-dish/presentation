import { useState } from 'react';

import { LoadingCard } from './LoadingCard';
import { PacientCard } from './PacientCard';

const showPacient = (pacient, setPacient) => {
    const [pacientInfo, setPacientInfo] = useState(null);
    const [pacientEncounters, setPacientEncounters] = useState(null);
    const [patientMedications, setPatientMedications] = useState([]);
    const [patientDiagnostics, setPatientDiagnostics] = useState([]);
    const [patientAllergies, setPatientAllergies] = useState([]);
    const [patientImages, setPatientImages] = useState([]);

    const [loading, setLoading] = useState(false);

    return (
        loading ?
        <LoadingCard />
        :
        <PacientCard
            pacient={pacient}
            pacientInfo={pacientInfo}
            pacientEncounters={pacientEncounters}
            patientDiagnostics={patientDiagnostics}
            patientMedications={patientMedications}
            patientAllergies={patientAllergies}
            patientImages={patientImages}
            setPacient={setPacient}
            setPacientInfo={setPacientInfo}
            setPacientEncounters={setPacientEncounters}
            setPatientDiagnostics={setPatientDiagnostics}
            setPatientMedications={setPatientMedications}
            setPatientAllergies={setPatientAllergies}
            setPatientImages={setPatientImages}
            setLoading={setLoading}
        />
    )
}

export const PacientView = ({ pacient, setPacient }) => {
    return (pacient ? showPacient(pacient, setPacient) : null);
}