import { useState } from 'react';

import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListSubheader from '@mui/material/ListSubheader';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import MedicationIcon from '@mui/icons-material/Medication';
import dateFormat from 'dateformat';

const pacientEncountersListStyles = {
    width: '100%',
    bgcolor: 'background.paper'
}

const validDate = (dateString, formatString) => {
    try {
        return dateFormat(dateString, formatString)
    } catch (error) {
        return "Not defined."
    }
}

export const MedicationsOnEncounterList = ({ medicationsOnEncounter }) => {
    const [openListItem, setOpenListItem] = useState(null);

    const handleOpenListItem = (clickedIndex) => {
        if (openListItem === clickedIndex) {
            setOpenListItem(null);
        } else {
            setOpenListItem(clickedIndex);
        }
    };

    return (
        <List
            sx={pacientEncountersListStyles}
            component="nav"
            aria-labelledby="pacient-medications-on-encounters"
        >
            {
                medicationsOnEncounter.map((medication, index) => (
                    <Box key={`medication-on-encounter-${index}`}>
                        { index > 0 ? <Divider /> : null }
                        <ListItemButton onClick={() => handleOpenListItem(`medication-on-encounter-${index}`)}>
                            <ListItemIcon>
                                <MedicationIcon />
                            </ListItemIcon>
                            <ListItemText primary={`Description: ${medication.description}`} />
                            {openListItem === `medication-on-encounter-${index}` ? <ExpandLess /> : <ExpandMore />}
                        </ListItemButton>
                        <Collapse in={openListItem === `medication-on-encounter-${index}`} timeout="auto" unmountOnExit>
                            <List component="div" disablePadding>
                                <ListItem>
                                    {`Description: ${medication.description}`}
                                </ListItem>
                                <ListItem>
                                    {`Code: ${medication.code}`}
                                </ListItem>
                                <ListItem>
                                    {`Reason Description: ${medication.reasonDescription}`}
                                </ListItem>
                                <ListItem>
                                    {`Start Date: ${validDate(medication.start, "dd/mm/yyyy")}`}
                                </ListItem>
                                <ListItem>
                                    {`End Date: ${validDate(medication.end, "dd/mm/yyyy")}`}
                                </ListItem>
                            </List>
                        </Collapse>
                    </Box>
                ))


            }
        </List>
    )
}