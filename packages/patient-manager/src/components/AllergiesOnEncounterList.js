import { useState } from 'react';

import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListSubheader from '@mui/material/ListSubheader';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import WarningIcon from '@mui/icons-material/Warning';
import dateFormat from 'dateformat';

const pacientEncountersListStyles = {
    width: '100%',
    bgcolor: 'background.paper'
}

const validDate = (dateString, formatString) => {
    try {
        return dateFormat(dateString, formatString)
    } catch (error) {
        return "Not defined."
    }
}


export const AllergiesOnEncounterList = ({ allergiesOnEncounter }) => {
    const [openListItem, setOpenListItem] = useState(null);

    const handleOpenListItem = (clickedIndex) => {
        if (openListItem === clickedIndex) {
            setOpenListItem(null);
        } else {
            setOpenListItem(clickedIndex);
        }
    };

    return (
        <List
            sx={pacientEncountersListStyles}
            component="nav"
            aria-labelledby="pacient-allergies-on-encounters"
        >
            {
                allergiesOnEncounter.map((allergy, index) => (
                    <Box key={`allergy-on-encounter-${index}`}>
                        { index > 0 ? <Divider /> : null }
                        <ListItemButton onClick={() => handleOpenListItem(`allergy-on-encounter-${index}`)}>
                            <ListItemIcon>
                                <WarningIcon />
                            </ListItemIcon>
                            <ListItemText primary={`${allergy.category}: ${allergy.description} (Code: ${allergy.code})`} />
                            {openListItem === `allergy-on-encounter-${index}` ? <ExpandLess /> : <ExpandMore />}
                        </ListItemButton>
                        <Collapse in={openListItem === `allergy-on-encounter-${index}`} timeout="auto" unmountOnExit>
                            <List component="div" disablePadding>
                                <ListItem>
                                    {`(${allergy.reaction1}) Severity: ${allergy.severity1}. Code: ${allergy.reaction1}`}
                                </ListItem>
                                <ListItem>
                                    {`(${allergy.reaction2}) Severity: ${allergy.severity2}. Code: ${allergy.reaction2}`}
                                </ListItem>
                                <ListItem>
                                    {`Start: ${validDate(allergy.start, "dd/mm/yyyy")}`}
                                </ListItem>
                                <ListItem>
                                    {`Stop: ${validDate(allergy.stop, "dd/mm/yyyy")}`}
                                </ListItem>
                            </List>
                        </Collapse>
                    </Box>
                ))


            }
        </List>
    )
}