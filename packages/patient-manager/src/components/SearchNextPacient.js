import { Button } from "@mui/material"

export const SearchNextPacient = ({ setPacient }) => {
    return (
        <Button variant="contained" onClick={() => setPacient(null)}>Search New Patient</Button>
    )
}