import { useState } from 'react';

import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListSubheader from '@mui/material/ListSubheader';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import HealingIcon from '@mui/icons-material/Healing';
import dateFormat from 'dateformat';

const pacientEncountersListStyles = {
    width: '100%',
    bgcolor: 'background.paper'
}

const validDate = (dateString, formatString) => {
    try {
        return dateFormat(dateString, formatString)
    } catch (error) {
        return "Not defined."
    }
}

export const ProceduresOnEncounterList = ({ proceduresOnEncounter }) => {
    const [openListItem, setOpenListItem] = useState(null);

    const handleOpenListItem = (clickedIndex) => {
        if (openListItem === clickedIndex) {
            setOpenListItem(null);
        } else {
            setOpenListItem(clickedIndex);
        }
    };

    return (
        <List
            sx={pacientEncountersListStyles}
            component="nav"
            aria-labelledby="pacient-procedures-on-encounters"
        >
            {
                proceduresOnEncounter.map((procedure, index) => (
                    <Box key={`procedure-on-encounter-${index}`}>
                        { index > 0 ? <Divider /> : null }
                        <ListItemButton onClick={() => handleOpenListItem(`procedure-on-encounter-${index}`)}>
                            <ListItemIcon>
                                <HealingIcon />
                            </ListItemIcon>
                            <ListItemText primary={`Code: ${procedure.code}, Reason code: ${procedure.reasonCode}`} />
                            {openListItem === `procedure-on-encounter-${index}` ? <ExpandLess /> : <ExpandMore />}
                        </ListItemButton>
                        <Collapse in={openListItem === `procedure-on-encounter-${index}`} timeout="auto" unmountOnExit>
                            <List component="div" disablePadding>
                                <ListItem>
                                    {`Start: ${validDate(procedure.start, "hh:mm, dd/mm/yyyy")}`}
                                </ListItem>
                                <ListItem>
                                    {`Stop: ${validDate(procedure.stop, "hh:mm, dd/mm/yyyy")}`}
                                </ListItem>
                            </List>
                        </Collapse>
                    </Box>
                ))


            }
        </List>
    )
}