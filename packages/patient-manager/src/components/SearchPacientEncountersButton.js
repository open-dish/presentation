
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';

import { getPatientEncounters } from '../api.js';
import { getPatientMedications } from '../api.js';
import { getPatientAllergies } from '../api.js';
import { getPatientDiagnostics } from '../api.js';
import { getPatientImages } from '../api.js';


const loadPacientEncounters = async (patientID, setPacientEncounters, setPatientMedications, setPatientAllergies, setPatientDiagnostics, setPatientImages, setLoading) => {
    
    const onEncountersLoaded = response => {setPacientEncounters(response.data);}
    const onMedicationsLoaded = response => {setPatientMedications(response.data);}
    const onAllergiesLoaded = response => {setPatientAllergies(response.data);}
    const onDiagnosticsLoaded = response => {setPatientDiagnostics(response.data);}
    const onImagesLoaded = response => {setPatientImages(response.data);}
    
    setLoading(true);
    await getPatientEncounters(patientID, onEncountersLoaded);
    await getPatientMedications(patientID, onMedicationsLoaded);
    await getPatientAllergies("87ce830a-2ed5-62fd-2879-2a6b62f6551a", onAllergiesLoaded);
    await getPatientDiagnostics(patientID, onDiagnosticsLoaded);
    await getPatientImages("53fd29a8-1e05-f923-9175-f544f77da5ac", onImagesLoaded);
        
    setLoading(false);
}

export const SearchPacientEncountersButton = ({ pacientInfo, setPacientEncounters, setPatientMedications, setPatientAllergies, setPatientDiagnostics, setPatientImages, setLoading }) => {
    return (
        <Tooltip title="Search patient encounters">
            <IconButton
                aria-label="Search patient's encounters"
                onClick={() => {loadPacientEncounters(pacientInfo.id, setPacientEncounters, setPatientMedications, setPatientAllergies, setPatientDiagnostics, setPatientImages, setLoading);}}
            >
                <ManageSearchIcon />
            </IconButton>
        </Tooltip>
    )
}