import { useState } from 'react';
import { Box } from '@mui/material';

import { NextPacient } from './NextPacient';
import { PacientView } from './PacientView';

export const PacientManager = () => {

    const [nextPacient, setNextPacient] = useState(null);

    return (
        <Box>
            <NextPacient pacient={nextPacient} setPacient={setNextPacient}/>
            <PacientView pacient={nextPacient} setPacient={setNextPacient}/>
        </Box>
    )
}