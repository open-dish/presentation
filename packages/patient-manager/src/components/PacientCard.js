import * as React from 'react';
import { useState } from 'react';

import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Avatar from '@mui/material/Avatar';
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import Typography from '@mui/material/Typography';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import dateFormat from 'dateformat';

import ArticleIcon from '@mui/icons-material/Article';
import WarningIcon from '@mui/icons-material/Warning';
import MedicationIcon from '@mui/icons-material/Medication';
import SimCardAlertIcon from '@mui/icons-material/SimCardAlert';
import ImageIcon from '@mui/icons-material/Image';

import { SearchPacientInfoButton } from './SearchPacientInfoButton';
import { SearchPacientEncountersButton } from './SearchPacientEncountersButton';
import { PacientEncountersList } from './PacientEncountersList';
import { SearchNextPacient } from './SearchNextPacient';

import { PatientMedicationsList } from './PatientMedicationsList';
import { PatientAllergiesList } from './PatientAllergiesList';
import { PatientDiagnosticsList } from './PatientDiagnosticsList';
import { PatientImagesList } from './PatientImagesList';


const validDate = (dateString, formatString) => {
    try {
        return dateFormat(dateString, formatString)
    } catch (error) {
        return "Not defined."
    }
}


function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography  component={'span'} >{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
  };
  
  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }


const showPacientInfo = (pacientInfo) => {
    return (
        <>
            <Typography variant="body2" color="text.secondary">
                Name: {pacientInfo.first} {pacientInfo.last} 
            </Typography>
            <Typography variant="body2" color="text.secondary">
                Birth: {validDate(pacientInfo.birthDate, "dd/mm/yyyy")}
            </Typography>
            <Typography variant="body2" color="text.secondary">
                Gender: {pacientInfo.gender}
            </Typography>
            <Typography variant="body2" color="text.secondary">
                Race: {pacientInfo.race}
            </Typography>
            <Typography variant="body2" color="text.secondary">
                Ethnicity: {pacientInfo.ethnicity}
            </Typography>
            <Typography variant="body2" color="text.secondary">
                Birth place: {pacientInfo.birthPlace}
            </Typography>
        </>
    )
}

const showPacientCardContent = (pacientInfo, pacientEncounters, patientDiagnostics, patientMedications, patientAllergies, patientImages) => {
    
    const [value, setValue] = React.useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    
    return (
        pacientInfo ?
        <CardContent>
            { pacientInfo ? showPacientInfo(pacientInfo) : null }
            
            { pacientEncounters ?
            <Box sx={{ width: '100%' }}>
                <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                    <Tabs value={value} onChange={handleChange} aria-label="Encounter table">
                    <Tab label={`Encounters (${pacientEncounters.length})`} {...a11yProps(0)} icon={<ArticleIcon />} iconPosition="start"/>
                    <Tab label={`Diagnostics (${patientDiagnostics.length})`} {...a11yProps(1)} icon={<SimCardAlertIcon />} iconPosition="start"/>
                    <Tab label={`Medications (${patientMedications.length})`} {...a11yProps(2)} icon={<MedicationIcon />} iconPosition="start"/>
                    <Tab label={`Allergies (${patientAllergies.length})`} {...a11yProps(3)} icon={<WarningIcon />} iconPosition="start"/>
                    <Tab label={`Images (${patientImages.length})`} {...a11yProps(4)} icon={<ImageIcon />} iconPosition="start"/>
                    </Tabs>
                </Box>
                <TabPanel value={value} index={0}>
                    {<PacientEncountersList pacientEncounters={pacientEncounters} />}
                </TabPanel>
                <TabPanel value={value} index={1}>
                    {<PatientDiagnosticsList patientDiagnostics={patientDiagnostics}/>}
                </TabPanel>
                <TabPanel value={value} index={2}>
                    {<PatientMedicationsList patientMedications={patientMedications}/>}
                </TabPanel>
                <TabPanel value={value} index={3}>
                    {<PatientAllergiesList patientAllergies={patientAllergies}/>}
                </TabPanel>
                <TabPanel value={value} index={4}>
                    {<PatientImagesList patientImages={patientImages}/>}
                </TabPanel>

            </Box>
            : null}


        </CardContent>
        :
        null
    )
}

const showPacientCardActions = (pacientEncounters, setPacient) => {
    return (
        pacientEncounters ? 
        <CardActions sx={{ justifyContent: "flex-end" }}>
            <SearchNextPacient setPacient={setPacient} />
        </CardActions>
        :
        null
    )
}

export const PacientCard = ({
    pacient,
    pacientInfo,
    pacientEncounters,
    patientDiagnostics, 
    patientMedications, 
    patientAllergies,
    patientImages,
    setPacient,
    setPacientInfo,
    setPacientEncounters,
    setPatientDiagnostics, 
    setPatientMedications, 
    setPatientAllergies,
    setPatientImages,
    setLoading
}) => {
    return (
        <Card>
            <CardHeader
                avatar={
                    <Avatar aria-label="paciente">
                        <PersonOutlineIcon />
                    </Avatar>
                }
                action={
                    pacientInfo ?
                    <SearchPacientEncountersButton 
                    
                    pacientInfo={pacientInfo}
                    setPatientDiagnostics={setPatientDiagnostics}
                    setPatientMedications={setPatientMedications}
                    setPatientAllergies={setPatientAllergies}
                    setPacientEncounters={setPacientEncounters}
                    setPatientImages={setPatientImages}
                    setLoading={setLoading}/>

                    :
                    <SearchPacientInfoButton pacientID={pacient.id} setPacientInfo={setPacientInfo} setLoading={setLoading}/>
                }
                title={
                    `Patient ${pacient.id}`
                }
                subheader={
                    pacientInfo ? `Check the information for patient ${pacient.id} below` : "Click on the magnifying-glass icon for more information"
                }
            />

            { showPacientCardContent(pacientInfo, pacientEncounters, patientDiagnostics, patientMedications, patientAllergies, patientImages) }
            { showPacientCardActions(pacientEncounters, setPacient) }
        </Card>
    )
}