import React from 'react';

import Box from '@mui/material/Box';
import Tooltip from '@mui/material/Tooltip';
import Fade from '@mui/material/Fade';
import { Typography } from '@mui/material';

// exemplo image = import blood_cells_1 from '../images/BloodImage_00000.jpg';
// exemplo imageDrawingMetadata:
// const imageDrawingMetadata = {
// 	width: 640,
// 	height: 480,
// 	boxes: [
// 		{
// 			x: 180,
// 			y: 295,
// 			width: 180,
// 			height: 170,
// 			borderWidth: 5,
// 			borderColor: "white",
// 			tooltip: {
// 				title: {
// 					text: `Célula não identificada.
// 					Apresenta cor roxa
// 					Requer análise manual
// 					`,
// 					style: {
// 						whiteSpace: "pre-line",
// 						fontSize: theme => theme.typography.pxToRem(12),
// 					}
// 				},
// 				placement: "bottom",
// 				hasArrow: true
// 			}
// 		}
// 	]
// }

const imageBaseURL = "";

const checkValue = (value) => {
	if (value === undefined || value === null)
		return 0;
	return Number(value);
}

const defineMainBoxStyle = (image) => {
	return {
		position: "relative",
		//backgroundImage: `url(${imageBaseURL}/${image.imagePath})`,
		backgroundImage: `url(https://raw.githubusercontent.com/Shenggan/BCCD_Dataset/master/BCCD/JPEGImages/BloodImage_00012.jpg)`,
		/*width: image.width,
		height: image.height*/
		width: 640,
		height: 480
	}
}

const defineImageBoxStyle = box => {
	return {
		position: "absolute",
		border: `4px solid`,
		borderColor: `rgb(${checkValue(box.confidence)*100}%,0%,0%)`,
		width: checkValue(box.bottomRight.x) - checkValue(box.topLeft.x),
		height: checkValue(box.bottomRight.y) - checkValue(box.topLeft.y),
		top:checkValue(box.topLeft.y),
		left: checkValue(box.topLeft.x),
		cursor: "pointer"
	}
}

const getImageBoxes = (boxes, i) => {
	return (
		boxes?.map((box, index) => (
			<>			
			{ box.bottomRight !== null &&
			<Tooltip
				key={`image-box-${i}-${index}`}
				title={
					<Typography>
						Label: { box.label }, Confidence: { Number(Number(box.confidence).toFixed(3)) }
					</Typography>	
				}
				//placement={box.tooltip.placement}
				TransitionComponent={Fade}
        		TransitionProps={{ timeout: 500 }}
				arrow={true}
			>
				<Box sx={defineImageBoxStyle(box)} />
			</Tooltip>
			}			
			</>
		))
	)
}

export const ImageDrawer = ({ image, index }) => {
	return (
		<Box
			sx={defineMainBoxStyle(image)}
		>
			{ getImageBoxes(image.boxesInfo, index) }
		</Box>
	);
}