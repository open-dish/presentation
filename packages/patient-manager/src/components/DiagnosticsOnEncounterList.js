import { useState } from 'react';

import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListSubheader from '@mui/material/ListSubheader';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import SimCardAlertIcon from '@mui/icons-material/SimCardAlert';
import CommentIcon from '@mui/icons-material/Comment';
import Paper from '@mui/material/Paper';

const pacientEncountersListStyles = {
    width: '100%',
    bgcolor: 'background.paper'
}

export const DiagnosticsOnEncounterList = ({ diagnosticsOnEncounter }) => {
    const [openListItemDiag, setOpenListItemDiag] = useState(null);
    const [openListItemObs, setOpenListItemObs] = useState(null);

    const handleOpenListItem = (clickedIndex, openListItem, setOpenListItem) => {
        if (openListItem === clickedIndex) {
            setOpenListItem(null);
        } else {
            setOpenListItem(clickedIndex);
        }
    };

    return (
        <List
            sx={pacientEncountersListStyles}
            component="nav"
            aria-labelledby="pacient-diagnostics-on-encounters"
        >
            {
                diagnosticsOnEncounter.map((diagnostic, index) => (
                    <Box key={`diagnostic-on-encounter-${index}`}>
                        { index > 0 ? <Divider /> : null }
                        <ListItemButton onClick={() => handleOpenListItem(`diagnostic-on-encounter-${index}`, openListItemDiag, setOpenListItemDiag)}>
                            <ListItemIcon>
                                <SimCardAlertIcon />                                
                            </ListItemIcon>
                            <ListItemText primary={`Status: ${diagnostic.status}. Interpreter: ${diagnostic.resultsInterpreter}`} />
                            {openListItemDiag === `diagnostic-on-encounter-${index}` ? <ExpandLess /> : <ExpandMore />}
                        </ListItemButton>
                        <Collapse in={openListItemDiag === `diagnostic-on-encounter-${index}`} timeout="auto" unmountOnExit>
                            <Paper variant="outlined">

                            {(diagnostic.conclusionCode).includes(271737000) &&
                                <ListItem>
                                    {`The patient has anemia. Reliability: ${diagnostic.reliability[0]}.`}
                                </ListItem>
                            }

                            {diagnostic.result.observation.length > 0 &&
                                <List component="div" disablePadding 
                                    subheader={
                                        <ListSubheader component="div" id="observations-list-subheader">
                                            Observations
                                        </ListSubheader>
                                    }
                                >                          

                                {  
                                diagnostic.result.observation.map((observation, index2) => (
                                    <Box key={`observation-medication-on-encounter-${index}-${index2}`}>
                                        { index2 > 0 ? <Divider/> : null }
                                        <ListItemButton onClick={() => handleOpenListItem(`observation-medication-on-encounter-${index}-${index2}`, openListItemObs, setOpenListItemObs)}>
                                            <ListItemIcon>
                                                <CommentIcon />                                
                                            </ListItemIcon>
                                            <ListItemText primary={`(Code: ${observation.code}) Category: ${observation.category}`} />
                                            {openListItemObs === `observation-medication-on-encounter-${index}-${index2}` ? <ExpandLess /> : <ExpandMore />}
                                        </ListItemButton>
                                        <Collapse in={openListItemObs === `observation-medication-on-encounter-${index}-${index2}`} timeout="auto" unmountOnExit>
                                            <List component="div" disablePadding>
                                                <ListItem>
                                                    {`Value: ${observation.value}`}
                                                </ListItem>
                                                <ListItem>
                                                    {`Units: ${observation.units}`}
                                                </ListItem>
                                                <ListItem>
                                                    {`Type: ${observation.type}`}
                                                </ListItem>
                                                </List>
                                        </Collapse>
                                    </Box>
                                ))
                                }                            
                                </List>
                            }
                            </Paper>
                        </Collapse>
                    </Box>
                ))


            }
        </List>
    )
}