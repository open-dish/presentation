import { useState } from 'react';

import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListSubheader from '@mui/material/ListSubheader';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import ImageIcon from '@mui/icons-material/Image';
import dateFormat from 'dateformat';
import { ImageDrawer } from './ImageDrawer';

const pacientEncountersListStyles = {
    width: '100%',
    bgcolor: 'background.paper'
}

const validDate = (dateString, formatString) => {
    try {
        return dateFormat(dateString, formatString)
    } catch (error) {
        return "Not defined."
    }
}

export const PatientImagesList = ({ patientImages }) => {
    const [openListItem, setOpenListItem] = useState(null);

    const handleOpenListItem = (clickedIndex) => {
        if (openListItem === clickedIndex) {
            setOpenListItem(null);
        } else {
            setOpenListItem(clickedIndex);
        }
    };

    return (
        <List
            sx={pacientEncountersListStyles}
            component="nav"
            aria-labelledby="pacient-images"
        >
            {
                patientImages.map((image, index) => (
                    
                    <Box key={`image-${index}`}>
                        { index > 0 ? <Divider /> : null }
                        <ListItemButton onClick={() => handleOpenListItem(`image-${index}`)}>
                            <ListItemIcon>
                                <ImageIcon />
                            </ListItemIcon>
                            <ListItemText primary={`${validDate(image.date, "yyyy, mmmm dS - hh:mm")}`} />
                            {openListItem === `image-${index}` ? <ExpandLess /> : <ExpandMore />}
                        </ListItemButton>
                        <Collapse in={openListItem === `image-${index}`} timeout="auto" unmountOnExit>
                            <List component="div" disablePadding>
                                <ListItem>
                                    {`Encounter: ${image.encounter}`}
                                </ListItem>
                                <ListItem>
                                    Image:
                                </ListItem>
                                <ListItem>
                                    <ImageDrawer image={image} index={`image-${index}`}/>
                                </ListItem>
                                <ListItem>
                                    {`Image Path: ${image.imagePath}`}
                                </ListItem>
                            </List>
                        </Collapse>
                    </Box>
                ))


            }
        </List>
    )
}