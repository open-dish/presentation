
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';

import { getPatientInfo } from '../api.js';

const loadPacientInfo = async (pacientID, setPacientInfo, setLoading) => {
    
    const onPacientInfoLoaded = response => {
        let info = {...response.data};
        info.gender = info.gender === 'F'? "Female" : "Male",
        setPacientInfo(info);
    }
    
    setLoading(true);
    await getPatientInfo(pacientID, onPacientInfoLoaded);
    setLoading(false);
}

export const SearchPacientInfoButton = ({ pacientID, setPacientInfo, setLoading }) => {
    return (
        <Tooltip title="Search patient information">
            <IconButton
                aria-label="Search patient information"
                onClick={() => loadPacientInfo(pacientID, setPacientInfo, setLoading)}
            >
                <SearchIcon />
            </IconButton>
        </Tooltip>
    )
}