import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import Skeleton from '@mui/material/Skeleton';

export const LoadingCard = () => {
    return (
        <Card>
            <CardHeader
                avatar={
                    <Skeleton animation="wave" variant="circular" width={40} height={40} />
                }
                action={
                    <Skeleton animation="wave" variant="circular" width={40} height={40} />
                }
                title={
                    <Skeleton animation="wave" width="30%" />
                }
                subheader={
                    <Skeleton animation="wave" width="40%" />
                }
            />
        </Card>
    )
}