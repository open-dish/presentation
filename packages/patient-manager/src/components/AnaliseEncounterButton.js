import { useState } from 'react';
import LoadingButton from '@mui/lab/LoadingButton';

import { getPatientEncounters } from '../api.js';


export const AnaliseEncounterButton = ({ Encounter, id, index, variant = null, EncountersML, setPacientEncountersML, pacientEncountersML }) => {
    const [loading, setLoading] = useState(false);

    const loadPacientEncounterML = async (Encounter, index, setLoading, setPacientEncountersML, pacientEncountersML) => {
        setLoading(true);
        await getEntries("4bcc0d3d-6598-421e-9dfd-1bcc16a561b0").then(data => {
            
            let EncountersML = [];
            if(pacientEncountersML==null)
                setPacientEncountersML({Encounters: []});
            else
                EncountersML = [...pacientEncountersML.Encounters];

            EncountersML[index] = data.data;
            
            setPacientEncountersML({Encounters: EncountersML});

        });
        setLoading(false);
    }

    return (
        <>
            <LoadingButton
                id={id}
                variant={ variant? variant : "contained" }
                onClick={() => loadPacientEncounterML(Encounter, index, setLoading, setPacientEncountersML, pacientEncountersML)}
                disabled={loading}
                loading={loading}
                size="small"
            >
                Analise Encounter
            </LoadingButton>

        </>

    )
}