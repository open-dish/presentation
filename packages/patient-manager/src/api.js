import axios from 'axios';

const baseURL = "https://dhim.api.open-dish.lab.ic.unicamp.br";
const baseURLSUnDaE = "https://sundae.api.open-dish.lab.ic.unicamp.br";


const defaultErrorHandler = error => {
    console.log(error);
}

export const getNextPacient = async (sucessCallback, errorCallback = null) => {
    return await axios.get(`${baseURLSUnDaE}/next-patient`)
    .then(sucessCallback)
    .catch(errorCallback ? errorCallback : defaultErrorHandler);
}

export const getPatientInfo = async (patientID, sucessCallback, errorCallback = null) => {
    return await axios.get(`${baseURL}/patient/${patientID}`)
    .then(sucessCallback)
    .catch(errorCallback ? errorCallback : defaultErrorHandler);
}

export const getPatientEncounters = async (patientID, sucessCallback, errorCallback = null) => {
    return await axios.get(`${baseURL}/encounter/patient/${patientID}`)
    .then(sucessCallback)
    .catch(errorCallback ? errorCallback : defaultErrorHandler);
}

export const getPatientMedications = async (patientID, sucessCallback, errorCallback = null) => {
    return await axios.get(`${baseURL}/medication/patient/${patientID}`)
    .then(sucessCallback)
    .catch(errorCallback ? errorCallback : defaultErrorHandler);
}

export const getPatientAllergies = async (patientID, sucessCallback, errorCallback = null) => {
    return await axios.get(`${baseURL}/allergy/patient/${patientID}`)
    .then(sucessCallback)
    .catch(errorCallback ? errorCallback : defaultErrorHandler);
}

export const getPatientDiagnostics = async (patientID, sucessCallback, errorCallback = null) => {
    return await axios.get(`${baseURL}/diagnostic/patient/${patientID}`)
    .then(sucessCallback)
    .catch(errorCallback ? errorCallback : defaultErrorHandler);
}

export const getPatientImages = async (patientID, sucessCallback, errorCallback = null) => {
    //return await axios.get(`${baseURL}/image/patient/${patientID}`)
    return await axios.get(`https://run.mocky.io/v3/1aad8200-2ac9-4f24-9dd3-b2e449ffa247`)
    .then(sucessCallback)
    .catch(errorCallback ? errorCallback : defaultErrorHandler);
}

export const getPatientMedicationsOnEncounter = async (encounterID, sucessCallback, errorCallback = null) => {
    return await axios.get(`${baseURL}/medication/encounter/${encounterID}`)
    .then(sucessCallback)
    .catch(errorCallback ? errorCallback : defaultErrorHandler);
}
export const getPatientProceduresOnEncounter = async (encounterID, sucessCallback, errorCallback = null) => {
    return await axios.get(`${baseURL}/procedure/encounter/${encounterID}`)
    .then(sucessCallback)
    .catch(errorCallback ? errorCallback : defaultErrorHandler);
}
export const getPatientAllergiesOnEncounter = async (encounterID, sucessCallback, errorCallback = null) => {
    return await axios.get(`${baseURL}/allergy/encounter/${encounterID}`)
    .then(sucessCallback)
    .catch(errorCallback ? errorCallback : defaultErrorHandler);
}
export const getPatientImagesOnEncounter = async (encounterID, sucessCallback, errorCallback = null) => {
    return await axios.get(`${baseURL}/image/encounter/${encounterID}`)
    //return await axios.get(`https://run.mocky.io/v3/1aad8200-2ac9-4f24-9dd3-b2e449ffa247`)
    .then(sucessCallback)
    .catch(errorCallback ? errorCallback : defaultErrorHandler);
}
export const getPatientDiagnosticsOnEncounter = async (encounterID, sucessCallback, errorCallback = null) => {
    return await axios.get(`${baseURL}/diagnostic/encounter/${encounterID}`)
    .then(sucessCallback)
    .catch(errorCallback ? errorCallback : defaultErrorHandler);
}



