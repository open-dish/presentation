const { merge } = require('webpack-merge');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const commonConfig = require('./webpack.common');
const packageJson = require('../package.json');

const prodConfig = {
  mode: 'production',
  output: {
    filename: '[name].[contenthash].js',
    publicPath: '/patient-manager/latest/',
  },
  plugins: [
    new ModuleFederationPlugin({
      name: 'patient_manager',
      filename: 'remoteEntry.js',
      exposes: {
        './PatientManagerApp': './src/bootstrap'
      },
      shared: packageJson.dependencies
    }),
  ],
};

module.exports = merge(commonConfig, prodConfig);